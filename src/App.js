import './App.css';
import Registration from './components/Registrastion';
import { ChakraProvider } from '@chakra-ui/react';
import Login from './components/Login';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './components/Home';

function App() {
  return (
    <Router>
      <ChakraProvider>
        <div className="App">
          <Switch>
            <Route path="/" exact>
              <Home />
            </Route>
            <Route path="/register">
              <Registration />
            </Route>
            <Route path="/login">
              <Login />
            </Route>
          </Switch>
        </div>
      </ChakraProvider>
    </Router>
  );
}

export default App;
