import React, { useState } from 'react';
import {
  FormControl,
  FormLabel,
  Input,
  FormHelperText,
  Button,
  FormErrorMessage,
} from '@chakra-ui/react';
import Page from './Page';

function validate({ name, email, password }) {
  let errors = {};
  if (!name) {
    errors.name = 'Required!!';
  }

  if (!email) {
    errors.email = 'Required!!';
  } else if (!/[A-Z0-9.%-+]+@[A-Z0-9]+\.[A-Z]{2,4}/i.test(email)) {
    errors.email = 'Invalid email!!';
  }

  if (!password) {
    errors.password = 'Required!!';
  }
  return [!Object.keys(errors).length, errors];
}

const Registration = () => {
  const [data, setData] = useState({
    name: '',
    email: '',
    password: '',
  });

  const [visited, setVisited] = useState({
    name: false,
    email: false,
    password: false,
  });

  const [error, setError] = useState({
    name: '',
    email: '',
    password: '',
  });

  function handleSubmit(e) {
    e.preventDefault();
    let [isInvalid, errObject] = validate(data);
    if (isInvalid) {
      //disable btn when submitting
      console.log('submitted');
      return;
    }
    setVisited({ name: true, email: true, password: true });
    setError({ ...errObject });
  }

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <FormControl id="name" isInvalid={visited.name && error.name}>
          <FormLabel>Name</FormLabel>
          <Input
            type="text"
            onChange={(e) => {
              setData({ ...data, name: e.target.value });
            }}
            onKeyUp={() => setError({ ...validate(data)[1] })}
            onBlur={(e) => {
              setVisited({ ...visited, name: true });
              setError({ ...validate(data)[1] });
            }}
            value={data.name}
          />
          <FormErrorMessage>{error.name}</FormErrorMessage>
        </FormControl>

        <FormControl id="email" isInvalid={visited.email && error.email}>
          <FormLabel>Email address</FormLabel>
          <Input
            type="email"
            onChange={(e) => {
              setData({ ...data, email: e.target.value });
            }}
            onKeyUp={() => setError({ ...validate(data)[1] })}
            onBlur={(e) => {
              setVisited({ ...visited, email: true });
              setError({ ...validate(data)[1] });
            }}
            value={data.email}
          />
          <FormHelperText>We'll never share your email.</FormHelperText>
          <FormErrorMessage>{error.email}</FormErrorMessage>
        </FormControl>

        <FormControl isInvalid={visited.password && error.password}>
          <FormLabel htmlFor="password">Password</FormLabel>
          <Input
            id="password"
            type="password"
            onChange={(e) => {
              setData({ ...data, password: e.target.value });
            }}
            onKeyUp={() => setError({ ...validate(data)[1] })}
            onBlur={(e) => {
              setVisited({ ...visited, password: true });
              setError({ ...validate(data)[1] });
            }}
            value={data.password}
          />
          <FormErrorMessage>{error.password}</FormErrorMessage>
        </FormControl>

        <div>
          <Button colorScheme="blue" type="submit">
            Register
          </Button>
        </div>
      </form>
    </div>
  );
};

function RegistrationPage() {
  return (
    <Page isRegister={true}>
      <Registration />
    </Page>
  );
}

export default RegistrationPage;
