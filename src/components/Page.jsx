import React, { Fragment } from 'react';
import Toggle from './Toggle';

const Page = ({ children, isRegister }) => {
  return (
    <Fragment>
      {children}
      <Toggle isRegister={isRegister} />
    </Fragment>
  );
};

export default Page;
