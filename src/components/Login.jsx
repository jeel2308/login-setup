import React, { Fragment } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import {
  FormControl,
  Input,
  FormLabel,
  FormErrorMessage,
  Button,
} from '@chakra-ui/react';
import Page from './Page';

let initialState = {
  name: '',
  password: '',
};

function validate({ name, password }) {
  let error = {};

  if (!name) {
    error.name = 'Required!!';
  }

  if (!password) {
    error.password = 'Required!!';
  }

  return error;
}

const onSubmit = (values) => {
  //disable btn when submitting
  console.log('submitted');
};

const Login = () => {
  return (
    <Formik
      initialValues={initialState}
      validate={validate}
      onSubmit={onSubmit}
    >
      {(formik) => {
        const { errors, touched } = formik;
        return (
          <Form>
            <FormControl isInvalid={errors['name'] && touched['name']}>
              <FormLabel htmlFor={'name'}>Name</FormLabel>
              <Field name="name">
                {({ field, meta }) => {
                  console.log(meta);
                  return (
                    <Fragment>
                      <Input id="name" type="text" {...field} />
                      <ErrorMessage name="name">
                        {() => (
                          <FormErrorMessage>
                            {meta.touched && meta.error}
                          </FormErrorMessage>
                        )}
                      </ErrorMessage>
                    </Fragment>
                  );
                }}
              </Field>
            </FormControl>
            <FormControl isInvalid={errors['password'] && touched['password']}>
              <FormLabel htmlFor="password"> Password</FormLabel>
              <Field name="password">
                {({ field, meta }) => {
                  return (
                    <>
                      <Input type="password" {...field} id="password" />
                      <ErrorMessage name="password">
                        {() => (
                          <FormErrorMessage>
                            {meta.touched && meta.error}
                          </FormErrorMessage>
                        )}
                      </ErrorMessage>
                    </>
                  );
                }}
              </Field>
            </FormControl>
            <div>
              <Button type="submit" colorScheme="blue">
                Log In
              </Button>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};

function LoginPage() {
  return (
    <Page isRegister={false}>
      <Login />
    </Page>
  );
}

export default LoginPage;
