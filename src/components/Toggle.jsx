import { Button } from '@chakra-ui/react';
import React from 'react';
import { Link } from 'react-router-dom';

function Toggle({ isRegister }) {
  return (
    <div>
      <Link to={isRegister ? '/login' : '/register'}>
        <Button colorScheme="blue">
          {isRegister
            ? 'Already have an account?Log In'
            : 'Do not have an account?Create One'}
        </Button>
      </Link>
    </div>
  );
}

export default Toggle;
